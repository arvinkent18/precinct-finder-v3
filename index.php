<?php

require_once 'core/Handler.php';
require_once 'core/Config.php';

use PrecinctFinder\core\Config;
use PrecinctFinder\core\Handler;

if (session_id() == '') {
    session_start();
}

    $handler = new Handler();
    $handler->getJavascriptAntiBot();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="description"
          content="COMELEC Precinct Finder is a PHP search form that similar to Google Autocomplete feature displays the result as you type">
    <meta name="keywords"
          content="COMELEC Precinct Finder, Autocomplete, Auto Suggest, PHP, HTML, CSS, jQuery, JavaScript, search form, MySQL, web component, responsive">
    <meta name="author" content="Arvin Kent S. Lazaga">

    <title>COMELEC Precinct Finder</title>

    <!-- Main Application Styles -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Live Search Styles -->
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animation.css">
    <!--[if IE 7]>
    <link rel="stylesheet" href="css/fontello-ie7.css">
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/ajaxlivesearch.min.css">
</head>
<body>
<!-- Application Title -->
<div id="app-title">
    <h2>COMELEC Precinct Finder</h2>
</div>
<!-- /Application Title -->

<!-- Search Form Demo -->
<div style="clear: both">
    <input type="text" class='mySearch' id="ls_query" placeholder="Enter Voter's Name or Address or Precinct Number ...">
</div>
<!-- /Search Form Demo -->

<div id="footer-title">
    &copy; Copyright 2019 
    <br />
    Designed and developed by Arvin Kent S. Lazaga of AdZU CITS
</div>

<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-1.11.1.min.js"></script>

<!-- Live Search Script -->
<script type="text/javascript" src="js/ajaxlivesearch.min.js"></script>

<script>
jQuery(document).ready(function(){
    jQuery(".mySearch").ajaxlivesearch({
        loaded_at: <?php echo time(); ?>,
        token: <?php echo "'" . $handler->getToken() . "'"; ?>,
        max_input: <?php echo Config::getConfig('maxInputLength'); ?>,
        onResultClick: function(e, data) {
            // get the index 0 (first column) value
            var selectedOne = jQuery(data.selected).find('td').eq('0').text();

            // set the input value
            jQuery('#ls_query').val(selectedOne);

            // hide the result
            jQuery("#ls_query").trigger('ajaxlivesearch:hide_result');
        },
        onResultEnter: function(e, data) {
            // do whatever you want
            // jQuery("#ls_query").trigger('ajaxlivesearch:search', {query: 'test'});
        },
        onAjaxComplete: function(e, data) {

        }
    });
})
</script>

</body>
</html>
